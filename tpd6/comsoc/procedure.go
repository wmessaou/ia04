package comsoc

import (
	"errors"
	"fmt"
)

type Alternative int
type Profile [][]Alternative
type Count map[Alternative]int

func Yooo() {
	fmt.Println("heee")
}

func rank(alt Alternative, prefs []Alternative) int {
	for i, p := range prefs {
		if alt == p {
			return i
		}
	}
	return -1
}

func maxCount(count Count) (bestAlts []Alternative) {
	temp := 0
	for k, v := range count {
		if v > temp {
			temp = v
			bestAlts = nil
			bestAlts = []Alternative{k}
		}
		if v == temp {
			bestAlts = append(bestAlts, k)
		}
	}
	return bestAlts
}

func minCount(count Count) (worstAlt []Alternative) {
	minScore := -1
	for alt, score := range count {
		if minScore == -1 {
			minScore = score
		}

		if score < minScore {
			minScore = score
			worstAlt = []Alternative{alt}
		} else if score == minScore {
			worstAlt = append(worstAlt, alt)
		}
	}

	return
}

func remove(slice []Alternative, s Alternative) []Alternative {
	for i, value := range slice {
		if value == s {
			return append(slice[:i], slice[i+1:]...)
		}
	}
	return slice
}

func checkPrefs(prefs [][]Alternative) error {
	for i := range prefs {
		if len(prefs[i]) != len(prefs[i+1]) && i != len(prefs)-2 {
			return errors.New("false")
		}
	}
	return errors.New("true")
}
func checkPrefAlternative(prefs [][]Alternative, alts []Alternative) bool {
	for i := range alts {
		apparait := 0
		for j := range prefs {
			for l := range prefs[j] {
				if alts[i] == prefs[j][l] {
					apparait++
				}
			}
			if apparait > 1 {
				return false
			}
		}
	}
	return true
}

func Contains(alts []Alternative, s Alternative) bool {
	for _, v := range alts {
		if v == s {
			return true
		}
	}
	return false
}
